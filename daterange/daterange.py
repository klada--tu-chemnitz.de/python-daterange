"""
Simple library which supports calculations with ranges of datetime.date.

(c) 2012 Daniel Klaffenbach <daniel.klaffenbach@cs.tu-chemnitz.de>
"""
import datetime

class DateRange(object):
    """
    Represents a date range.
    
    The attribute `from_date` is the first day of the range, `to_date` is the
    last day. Comparisons of DateRange objects will always compare the
    `from_date` of the objects and not the number of days. 
    """
    __slots__ = ['from_date', 'to_date']
    
    def __init__(self, from_date, to_date):
        if not isinstance(from_date, datetime.date) or not isinstance(to_date, datetime.date):
            raise TypeError('Invalid date provided.')
        if from_date > to_date:
            raise ValueError('The beginning of the date range cannot be greater than the end of the date range.')
        self.from_date = from_date
        self.to_date = to_date
    
    def __eq__(self, date_range):
        if not isinstance(date_range, self.__class__):
            raise TypeError
        return self.from_date == date_range.from_date and self.to_date == date_range.to_date

    def __ge__(self, date_range):
        return self > date_range or self == date_range
    
    def __gt__(self, date_range):
        if not isinstance(date_range, self.__class__):
            raise TypeError
        return self.from_date > date_range.from_date or (self.from_date == date_range.from_date and self.to_date > date_range.to_date)

    def __le__(self, date_range):
        return self < date_range or self == date_range
    
    def __lt__(self, date_range):
        """
        Checks if DateRange instance a starts earlier than DateRange instance b.
        """
        if not isinstance(date_range, self.__class__):
            raise TypeError
        return self.from_date < date_range.from_date or (self.from_date == date_range.from_date and self.to_date < date_range.to_date)

    def __repr__(self):
        return str(self)

    def __setattr__(self, attr, value):
        """
        DateRange instances are immutable, so we support setting attributes
        only once during initialization.
        """
        try:
            getattr(self, attr)
        except AttributeError:
            # The value has not been initialized, so we allow setting it this time only
            super(DateRange, self).__setattr__(attr, value)
        else:
            raise AttributeError('Cannot set attributes')

    def __str__(self):
        return "%s - %s" %(self.from_date, self.to_date)

    @property
    def days(self):
        """
        Number of days of this DateRange, including the days specified by
        `from_date` and `to_date`.
        """
        delta = self.to_date-self.from_date
        return delta.days + 1

"""
Simple library which supports calculations with ranges of datetime.date.

(c) 2012 Daniel Klaffenbach <daniel.klaffenbach@cs.tu-chemnitz.de>
"""
import datetime
from threading import Lock
from daterange import DateRange

class DateRangeList(object):
    """
    A container class with a list-like interface which stores DateRange
    objects and offers some common operations on them.
    
    @TODO: Improve performance
    """ 
    __slots__ = ['__ranges', '__lock']
    
    def __init__(self, *args):
        # The lock is required for thread-safety
        self.__lock = Lock()
        self.__ranges = []
        for i in args:
            self.add(i)
    
    def __contains__(self, item):
        if not isinstance(item, DateRange):
            raise TypeError
        for i in self.__ranges:
            if item.from_date >= i.from_date and item.to_date <= i.to_date:
                return True
            elif item.to_date < i.to_date:
                return False
        return False
    
    def __getitem__(self, item):
        return self.__ranges.__getitem__(item)
    
    def __iter__(self):
        return self.__ranges.__iter__()
    
    def __len__(self):
        return len(self.__ranges)
    
    def __repr__(self):
        return str(self.__ranges)

    def __nonzero__(self):
        if self.__ranges:
            return True
        else:
            return False

    def add(self, date_range):
        """
        Adds the element and makes sure that the list stays ordered and as
        small as possible (which means merging overlapping date ranges).
        """
        if not isinstance(date_range, DateRange):
            raise TypeError
        
        self.__lock.acquire()
        
        # Insert the DateRange in the proper position
        index_insert = 0
        for i in self.__ranges:
            if i < date_range:
                index_insert += 1
            else:
                break
        
        self.__ranges.insert(index_insert, date_range)

        # Now we are compressing the list of DateRanges, which means merging
        # overlapping ranges together
        compressed = []
        
        from_date = None
        to_date = None
        
        for i in self.__ranges:
            if not from_date:
                from_date = i.from_date
                to_date = i.to_date
            else:
                if i.from_date > to_date + datetime.timedelta(days=1):
                    compressed.append(DateRange(from_date, to_date))
                    from_date = i.from_date
                    to_date = i.to_date
                elif i.to_date < to_date:
                    pass
                else:
                    to_date = i.to_date
        if from_date:
            compressed.append(DateRange(from_date, to_date))
        self.__ranges = compressed
        self.__lock.release()
    
    @property
    def days(self):
        """
        The number of *unique* days in this list's DateRanges.
        """
        days = 0
        for i in self:
            days += i.days
        return days
    
    def intersection(self, range_list):
        """
        Returns the date ranges which intersect between this DateRangeList and
        the list passed in to this method.
        
        @rtype: DateRangeList
        """
        if not isinstance(range_list, self.__class__):
            raise TypeError
               
        if not self or not range_list:
            return self.__class__()
                
        intersection = []
        range_list_index = 0
        for i in self:
            for j in range_list[range_list_index:]:
                if j.to_date < i.from_date:
                    # out of range, skip to next range and do not check this range (j) again
                    range_list_index += 1
                elif j.from_date > i.to_date:
                    # out of range and will never cross, as the date ranges are ordered
                    break
                else:
                    # to_date is in range
                    start_intersection = max(i.from_date, j.from_date)
                    end_intersection = min(i.to_date, j.to_date)
                    intersection.append(DateRange(start_intersection, end_intersection))

        return self.__class__(*intersection)
    
    @property
    def ranges(self):
        return list(self.__ranges)

#!/usr/bin/env python
import datetime
import unittest
from daterange import DateRange
from daterangelist import DateRangeList

class DateRangeTest(unittest.TestCase):

    def test_attributes(self):
        i = DateRange(datetime.date(2000,1,1), datetime.date(2000,2,1))
        self.assertEqual(i.from_date, datetime.date(2000,1,1))
        self.assertEqual(i.to_date, datetime.date(2000,2,1))
    
    def test_comparison(self):
        a       = DateRange(datetime.date(2000,1,1), datetime.date(2000,2,1))
        a_equal = DateRange(datetime.date(2000,1,1), datetime.date(2000,2,1))
        b       = DateRange(datetime.date(2000,1,2), datetime.date(2000,2,1))
        
        self.assertEqual(a, a_equal)
        self.assertNotEqual(a, b)
        self.assertTrue(a<b)
        self.assertTrue(a<=b)
        self.assertTrue(a<=a_equal)
        self.assertTrue(b>a)
        self.assertTrue(b>=a)
        
        self.assertFalse(a>b)
        self.assertFalse(b<a)
    
    def test_days(self):
        i = DateRange(datetime.date(2000,1,1), datetime.date(2000,1,31))
        self.assertEqual(i.days, 31)
        # leap year february
        i = DateRange(datetime.date(2000,2,1), datetime.date(2000,3,1))
        self.assertEqual(i.days, 30)

    def test_immutable_object(self):
        i = DateRange(datetime.date(2000,1,1), datetime.date(2000,2,1))
        try:
            i.from_date = datetime.date(2000,1,2)
        except AttributeError:
            pass
        else:
            self.fail()
        try:
            i.to_date = datetime.date(2000,1,2)
        except AttributeError:
            pass
        else:
            self.fail()          

    def test_invalid_parameters(self):
        self.assertRaises(TypeError, DateRange, 'a', 'b')
        self.assertRaises(ValueError, DateRange, datetime.date(2000,2,1), datetime.date(2000,1,1)) 


class DateRangeListTest(unittest.TestCase):
    
    def test_add(self):
        """ Test adding of DateRanges through the constructor and through add() """
        l = DateRangeList(DateRange(datetime.date(2000,1,1), datetime.date(2000,1,31)))
        l.add(DateRange(datetime.date(2000,5,1), datetime.date(2000,5,31)))
        self.assertEqual(len(l), 2)
    
    def test_autosorted(self):
        """ Test if ranges added in arbitrary order produce a ordered list """
        l=DateRangeList()
        l.add(DateRange(datetime.date(2000,5,1), datetime.date(2000,5,31)))
        l.add(DateRange(datetime.date(2012,1,1), datetime.date(2012,1,31)))
        l.add(DateRange(datetime.date(2002,3,1), datetime.date(2002,3,31)))
        l.add(DateRange(datetime.date(2000,1,1), datetime.date(2000,3,1)))
        
        # The same DateRanges as above, but in order
        py_list = [
            DateRange(datetime.date(2000,1,1), datetime.date(2000,3,1)),
            DateRange(datetime.date(2000,5,1), datetime.date(2000,5,31)),
            DateRange(datetime.date(2002,3,1), datetime.date(2002,3,31)),
            DateRange(datetime.date(2012,1,1), datetime.date(2012,1,31))
        ]
        
        self.assertEquals(l.ranges, py_list)
    
    def test_contains(self):
        """ Test the *date_range in date_range_list* functionality """
        drl = DateRangeList()
        drl.add(DateRange(datetime.date(2000,8,1), datetime.date(2008,8,31)))
        drl.add(DateRange(datetime.date(2000,1,1), datetime.date(2000,3,1)))
        drl.add(DateRange(datetime.date(2000,4,1), datetime.date(2000,4,30)))
        
        self.assertFalse(DateRange(datetime.date(2000,3,1), datetime.date(2000,4,5)) in drl)
        
        drl.add(DateRange(datetime.date(2000,2,15), datetime.date(2000,3,31)))
        self.assertTrue(DateRange(datetime.date(2000,3,1), datetime.date(2000,4,5)) in drl)
    
    def test_days(self):
        drl = DateRangeList()
        drl.add(DateRange(datetime.date(2000,1,1), datetime.date(2000,1,31)))
        drl.add(DateRange(datetime.date(2000,1,15), datetime.date(2000,2,15)))
        self.assertEqual(drl.days, 31+15)
    
    def test_intersection(self):
        drl_a = DateRangeList()
        drl_a.add(DateRange(datetime.date(2000,1,1), datetime.date(2000,3,1)))
        drl_a.add(DateRange(datetime.date(2000,4,15), datetime.date(2000,4,30)))
        drl_a.add(DateRange(datetime.date(2000,6,1), datetime.date(2000,7,31)))
        
        drl_b = DateRangeList()
        drl_b.add(DateRange(datetime.date(1999,12,1), datetime.date(2000,2,10)))
        drl_b.add(DateRange(datetime.date(2000,4,20), datetime.date(2000,6,15)))
        drl_b.add(DateRange(datetime.date(2000,12,1), datetime.date(2000,12,31)))
        
        self.assertEqual(drl_a.intersection(drl_b).ranges, drl_b.intersection(drl_a).ranges)
        
        intersection_list = [
            DateRange(datetime.date(2000,1,1), datetime.date(2000,2,10)),
            DateRange(datetime.date(2000,4,20), datetime.date(2000,4,30)),
            DateRange(datetime.date(2000,6,1), datetime.date(2000,6,15)),
        ]
        self.assertEqual(drl_a.intersection(drl_b).ranges, intersection_list)

    def test_merged_ranges(self):
        """ Test merging of overlapping ranges """
        drl = DateRangeList()
        drl.add(DateRange(datetime.date(2000,8,1), datetime.date(2008,8,31)))
        drl.add(DateRange(datetime.date(2000,1,1), datetime.date(2000,3,1)))
        drl.add(DateRange(datetime.date(2000,4,1), datetime.date(2000,4,30)))
        drl.add(DateRange(datetime.date(2000,6,1), datetime.date(2000,6,30)))
        
        # nothing should be merged so far
        self.assertEqual(len(drl), 4)
        
        # this is the first date range which overlaps with existing ranges -> merge required
        drl.add(DateRange(datetime.date(2000,2,15), datetime.date(2000,3,31)))
        self.assertEqual(len(drl), 3)
        
        ranges_lst = [
            DateRange(datetime.date(2000,1,1), datetime.date(2000,4,30)),
            DateRange(datetime.date(2000,6,1), datetime.date(2000,6,30)),
            DateRange(datetime.date(2000,8,1), datetime.date(2008,8,31))
        ]
        self.assertEqual(drl.ranges, ranges_lst)
        
        drl.add(DateRange(datetime.date(2000,3,15), datetime.date(2000,7,15)))
        ranges_lst = [
            DateRange(datetime.date(2000,1,1), datetime.date(2000,7,15)),
            DateRange(datetime.date(2000,8,1), datetime.date(2008,8,31))
        ]
        self.assertEqual(drl.ranges, ranges_lst)
        
        drl.add(DateRange(datetime.date(1999,12,1), datetime.date(2010,1,15)))
        self.assertEqual(drl.ranges, [DateRange(datetime.date(1999,12,1), datetime.date(2010,1,15))])
        

if __name__ == "__main__":
    unittest.main()

#!/usr/bin/env python

from distutils.core import setup

setup(name='daterange',
    version='1.0',
    description='Simple library which supports calculations with ranges of datetime.date.',
    author='Daniel Klaffenbach',
    author_email='daniel.klaffenbach@cs.tu-chemnitz.de',
    url='https://gitlab.hrz.tu-chemnitz.de/klada--tu-chemnitz.de/python-daterange',
    packages=['daterange'],
)
